package com.hz.controller;

import com.hz.pojo.SfUser;
import com.hz.service.UserDaoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/UserDaoController")
public class UserDaoController {

    @Resource
    private UserDaoService userDaoService;

    @RequestMapping("/getSfUsers")
    public SfUser getSfUsers() {
        System.out.println("======");
        return userDaoService.getSfUsers();
    }
}
