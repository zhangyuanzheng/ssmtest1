package com.hz.pojo;

/**
 * sf_address地址表
 * 映射的收货地址类
 */
public class SfAddress {

    private long address_id;//地址ID
    private String address_name;//地址姓名

    public long getAddress_id() {
        return address_id;
    }

    public void setAddress_id(long address_id) {
        this.address_id = address_id;
    }

    public String getAddress_name() {
        return address_name;
    }

    public void setAddress_name(String address_name) {
        this.address_name = address_name;
    }
}
