package com.hz.pojo;

/**
 * sf_admin管理员表
 * 映射的实体类
 */
public class SfAdmin {

    private long admin_id;//管理员ID
    private String admin_name;//管理姓名
     private SfRole admin_role;//管理员角色

    public long getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(long admin_id) {
        this.admin_id = admin_id;
    }

    public String getAdmin_name() {
        return admin_name;
    }

    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }

    public SfRole getAdmin_role() {
        return admin_role;
    }

    public void setAdmin_role(SfRole admin_role) {
        this.admin_role = admin_role;
    }

    @Override
    public String toString() {
        return "SfAdmin{" +
                "admin_id=" + admin_id +
                ", admin_name='" + admin_name + '\'' +
                ", admin_role=" + admin_role.toString() +
                '}';
    }
}
