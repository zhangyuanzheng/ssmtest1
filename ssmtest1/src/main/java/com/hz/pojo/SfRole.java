package com.hz.pojo;


/**
 * sf_role角色表映射的实体类
 */
public class SfRole {
    private long role_id;//角色ID
    private String role_name;//角色姓名
    private String role_remarks;//角色备注

    public long getRole_id() {
        return role_id;
    }

    public void setRole_id(long role_id) {
        this.role_id = role_id;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }

    public String getRole_remarks() {
        return role_remarks;
    }

    public void setRole_remarks(String role_remarks) {
        this.role_remarks = role_remarks;
    }

    @Override
    public String toString() {
        return "SfRole{" +
                "role_id=" + role_id +
                ", role_name='" + role_name + '\'' +
                ", role_remarks='" + role_remarks + '\'' +
                '}';
    }
}
