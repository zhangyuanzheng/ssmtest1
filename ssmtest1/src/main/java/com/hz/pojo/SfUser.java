package com.hz.pojo;

import java.util.List;


/**
 * sf_address表映射的用户实体类
 */
public class SfUser {
private long user_id;
private String user_name;
private String   user_password;
private Integer  user_sex;
private Integer  user_state;
private List<SfAddress> listaddress;

    public List<SfAddress> getListaddress() {
        return listaddress;
    }

    public void setListaddress(List<SfAddress> listaddress) { this.listaddress = listaddress;
    }

    public SfUser(long user_id, String user_name, String user_password, int user_sex, int user_state) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_password = user_password;
        this.user_sex = user_sex;
        this.user_state = user_state;
    }

    public SfUser() {
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public Integer getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(Integer user_sex) {
        this.user_sex = user_sex;
    }

    public Integer getUser_state() {
        return user_state;
    }

    public void setUser_state(Integer user_state) {
        this.user_state = user_state;
    }

    @Override
    public String toString() {
        return "SfUser{" +
                "user_id=" + user_id +
                ", user_name='" + user_name + '\'' +
                ", user_password='" + user_password + '\'' +
                ", user_sex=" + user_sex +
                ", user_state=" + user_state +
                '}';
    }
}
