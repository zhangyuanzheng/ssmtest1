package com.hz.service.impl;

import com.hz.dao.UserDao;
import com.hz.pojo.SfUser;
import com.hz.service.UserDaoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("userDaoService")
public class UserDaoServiceImpl implements UserDaoService {

    @Resource
    private UserDao userDao;

    @Override
    public SfUser getSfUsers() {
        return userDao.getSfUsers();
    }
}
